## A simple `echo` function to get started with GitLab Serverless.

See the [documentation](https://docs.gitlab.com/ee/user/project/clusters/serverless/) for more information.